const express = require("express");
const router = express.Router();
const mailingController = require("../controllers/mailingController");

router.get("/mailing", mailingController.getAll);

module.exports = router;
