const productController = require("../controllers/productsController");
const Product = require("../models/productModel");

// Mocking the Product.find method
jest.mock("../models/productModel", () => ({
  find: jest.fn(),
  findOne: jest.fn(),
}));

describe("getAll", () => {
  it("should return all products", async () => {
    const mockProducts = [{ name: "Product 1" }, { name: "Product 2" }];
    Product.find.mockResolvedValue(mockProducts);

    const req = {};
    const res = {
      json: jest.fn(),
    };
    const next = jest.fn();

    await productController.getAll(req, res, next);

    expect(res.json).toHaveBeenCalledWith({ products: mockProducts });
  });

  it("should handle errors", async () => {
    Product.find.mockRejectedValue(new Error("Error fetching products"));

    const req = {};
    const res = {
      json: jest.fn(),
    };
    const next = jest.fn();

    await productController.getAll(req, res, next);

    expect(next).toHaveBeenCalledWith(expect.any(Error));
  });
});

describe("getById", () => {
  it("should return a product by ID", async () => {
    const mockProduct = { id: 1, name: "Product 1" };
    Product.findOne.mockResolvedValue(mockProduct);

    const req = {
      params: { id: "1" },
    };
    const res = {
      json: jest.fn(),
    };
    const next = jest.fn();

    await productController.getById(req, res, next);

    expect(res.json).toHaveBeenCalledWith({ product: mockProduct });
  });

  it("should handle errors when product is not found", async () => {
    Product.findOne.mockResolvedValue(null);

    const req = {
      params: { id: "2" },
    };
    const res = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
    };
    const next = jest.fn();

    await productController.getById(req, res, next);
    console.log(res.json);
    expect(res.status).toHaveBeenCalledWith(404);
    expect(res.json).toHaveBeenCalledWith({
      message: "Couldn't find product by id 2",
    });
  });
});
