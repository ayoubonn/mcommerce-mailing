const nodemailer = require("nodemailer");

async function sendEmail(recipient, subject, body) {
  let transporter = nodemailer.createTransport({
    host: process.env.EMAIL_HOST,
    port: 465,
    secure: true,
    auth: {
      user: process.env.EMAIL_USER,
      pass: process.env.EMAIL_PASSWORD,
    },
  });

  let info = await transporter.sendMail({
    from: process.env.EMAIL_USER,
    to: recipient,
    subject: subject,
    text: body,
  });

  console.log("Email sent: %s", info.messageId);
}

module.exports = { sendEmail };
