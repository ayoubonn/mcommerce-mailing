const amqp = require("amqplib");

async function startConsumer(queueName, onMessage) {
  try {
    const connection = await amqp.connect(process.env.RABBITMQ_URL);
    const channel = await connection.createChannel();
    await channel.assertQueue(queueName);

    channel.consume(queueName, (message) => {
      if (message !== null) {
        console.log("Received message:", message.content.toString());
        onMessage(JSON.parse(message.content.toString()));
        channel.ack(message);
      }
    });
  } catch (error) {
    console.error("Error in RabbitMQ consumer:", error);
  }
}

module.exports = { startConsumer };
