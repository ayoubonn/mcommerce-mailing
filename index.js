require("dotenv").config();
const { startConsumer } = require("./rabbitmq");
const { sendEmail } = require("./emailSender");

// Start the RabbitMQ consumer
startConsumer("payments", async (message) => {
  // Assuming message contains { recipient, subject, body }
  try {
    await sendEmail(message.recipient, message.subject, message.body);
  } catch (error) {
    console.error("Failed to send email:", error);
  }
});

console.log("Server is running...");
