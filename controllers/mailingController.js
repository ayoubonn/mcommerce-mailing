const Product = require("../models/productModel");

async function getAll(req, res, next) {
  try {
    const result = await Product.find();
    res.json({ products: result });
  } catch (err) {
    console.error(`Error while getting products`, err.message);
    next(err);
  }
}
async function getById(req, res, next) {
  {
    const result = await Product.findOne({ id: parseInt(req.params.id) });
    if (result) {
      res.json({ product: result });
    } else {
      console.error(`Error while getting product by id ` + req.params.id);
      res
        .status(404)
        .json({ message: `Couldn't find product by id ` + req.params.id });
    }
  }
}

module.exports = {
  getById,
  getAll,
};
